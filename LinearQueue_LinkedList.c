#include<stdio.h>
#include<stdlib.h>
struct QNode {
	char data;
	struct QNode* next;
};
/*  Hai biến toàn cục dùng để chứa địa chỉ của Front and Rear. */ 
struct QNode* front = NULL;
struct QNode* rear = NULL;

/* Chèn một ký tự vào trong hàng đợi */
void Enqueue(char c) {
	struct QNode* temp = 
		(struct QNode*)malloc(sizeof(struct QNode));
	temp->data = c; 
	temp->next = NULL;
	if(front == NULL && rear == NULL){
		front = rear = temp;
		return;
	}
	rear->next = temp;
	rear = temp;
}

/* Lấy dữ liệu ra khỏi hàng đợi từ vị trí front */
void Dequeue() {
	struct QNode* temp = front;
	if(front == NULL) {
		printf("Queue is Empty\n");
		return;
	}
	if(front == rear) {
		front = rear = NULL;
	}
	else {
		front = front->next;
	}
	free(temp);
}

/* In ra tất cả các phần tử trogn hàng đợi */
void PrintQ() {
	struct QNode* temp = front;
	while(temp != NULL) {
		printf("%c ",temp->data);
		temp = temp->next;
	}
	printf("\n");
}

int main(){
    /* Hàm kiểm tra hoạt động của toàn bộ chương trinh
       In kết quả sau mỗi bước thực hiện */
    Enqueue('A');  PrintQ();  
    Enqueue('B');  PrintQ();  
    Enqueue('C');  PrintQ();
    Dequeue();	  PrintQ();
    Dequeue();	  PrintQ();
    Enqueue('D');  PrintQ();
    Enqueue('E');  PrintQ();

    return 0;
}
