
#include <iostream>
using namespace std;
#define SIZE 500

class Stack
{
private:
         int A[SIZE];  /* Mảng để chứa ngăn xếp */
         int top;   /* Biến đánh dấu vị trí đỉnh của ngăn xếp */
public:
	/* Hàm khởi tạo của lớp Stack */
	Stack()
	{
		top = 0; /* Khi stack rỗng, top = 0 */
	}

	/* Chèn phần tử vào đỉnh stack. */ 
	void Push(int n) 
	{
	  if(top == SIZE) { /* Xử lý ngoại lệ khi stack bị tràn */ 
                            cout << "ERROR: Stack overflow!" << endl;
			return;
		}
		A[top++] = n;
	}
 
	/* Xóa bỏ phần tử từ đỉnh của stack.*/
	void Pop() 
	{
		if(top == 0) { /* Kiểm tra nếu stack rỗng */
			cout << "ERROR: Stack is empty" << endl;
			return;
		}
		top--;
	}
 
	int Top() 
	{
		return A[--top]; /* Trả về phần tử ở đỉnh của stack */
	}
 
	/* Kiểm tra stack có rỗng hay không, nếu rỗng trả về true, ngược lại 
              trả về false */
	bool isEmpty()
	{
		if(top == 0) return true;
		return false;
	}

	/* HÀM IN RA CÁC PH ̀N TỬ CỦA STACK */
	void Print() {
		int i;
		for(i = 0; i<top; i++)
			cout << A[i] << " ";
		cout << endl;
	}
};

int main()
{
    /* Code để kiểm tra hoạt động của chương trình,
    gọi hàm Print() sau mỗi bước để kiểm tra tính đúng đắn */
	Stack S;
	S.Push(1); /* 1 */
	S.Push(2); /* 1, 2 */
	S.Print(); 
	S.Push(5); /* 1, 2, 5 */
	S.Print();
	S.Pop();    /* 1, 2 */
	S.Print();
	
	return 0;
}
