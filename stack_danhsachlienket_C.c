
#include <stdio.h>
#include <stdlib.h>

#define INT_MIN -2147483648
#define INT_MAX 2147483648

struct sNode
{
	int data;
	struct sNode* next;
};

struct sNode* newNode(int data)
{
	struct sNode* s_node =
        	(struct sNode*) malloc(sizeof(struct sNode));
	s_node->data = data;
	s_node->next = NULL;
	return s_node;
}

int isEmpty(struct sNode *root)
{
	return !root;
}

void Push(struct sNode** root, int data)
{
	struct sNode* s_node = newNode(data);
	s_node->next = *root;
	*root = s_node;
}

void Pop(struct sNode** root)
{
	if (isEmpty(*root)){
    	printf("ERROR: Stack rong\n");
               	return;
      	}
	struct sNode* temp = *root;
	*root = (*root)->next;
	int popped = temp->data;
	free(temp);
}

int Top(struct sNode* root)
{
	if (isEmpty(root)) {
    	printf("ERROR: Stack rong\n");
               	return INT_MIN;
      	}
	return root->data;
}


int main()
{
	struct sNode* root = NULL;

	Push(&root, 10);
	Push(&root, 20);
	Push(&root, 30);

	printf("%d da duoc xoa khoi stack\n",Top(root));
	Pop(&root);
	printf("phan tu dinh stack la %d\n", Top(root));

	return 0;
}
